/*
Similar to standard lib's log.Logger, but with log level filtering.

Logs into bufio.Writer, need to flush in the end.
*/
package quietlog

import (
	"bufio"
	"io"
	"io/ioutil"
	"log"
	"os"
)

const (
	LEVEL_DEBUG	= iota
	LEVEL_TRACE
	LEVEL_INFO
	LEVEL_WARN
	LEVEL_ERROR
)

var LEVEL_PREFIXES	= [] string { "DEBUG\t", "TRACE\t", "INFO\t", "WARN\t", "ERROR\t" }


type Logger struct {
	Stream	*bufio.Writer
	Loggers	[] *log.Logger
	DEBUG, TRACE, INFO, WARN, ERROR	*log.Logger
	LevelPrefixes	*[] string
}

func New( logLvl  byte, outputStream  io.Writer ) ( logger  *Logger ) {
	logger	= new( Logger )
	logger.Loggers	= make( [] *log.Logger, len( LEVEL_PREFIXES ) )
	logger.LevelPrefixes	= & LEVEL_PREFIXES

	if outputStream == nil {
		outputStream	= os.Stdout
	}
	logger.Stream	= bufio.NewWriterSize( outputStream, 1024 * 1024 )
	logger.SetLevel( logLvl )
	return
}

func ( self  *Logger ) SetLevel( logLvl  byte ) {
	var (
		flags	= log.LstdFlags
		i	byte
	)

	if int( logLvl ) > len( *self.LevelPrefixes ) {
		logLvl	= byte( len( *self.LevelPrefixes ) )
	}

	for ; i < logLvl; i++ {
		self.Loggers[ i ] = log.New( ioutil.Discard, ( *self.LevelPrefixes )[ i ], flags )
	}

	for logLvl = byte( len( *self.LevelPrefixes ) ); i < logLvl; i++ {
		self.Loggers[ i ] = log.New( self.Stream, ( *self.LevelPrefixes )[ i ], flags )
	}

	self.DEBUG	= self.Loggers[ LEVEL_DEBUG ]
	self.TRACE	= self.Loggers[ LEVEL_TRACE ]
	self.INFO	= self.Loggers[ LEVEL_INFO ]
	self.WARN	= self.Loggers[ LEVEL_WARN ]
	self.ERROR	= self.Loggers[ LEVEL_ERROR ]
}